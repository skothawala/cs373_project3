package com.skkers.patterns.observer.model;

public class AndroidSubscriber implements Observer {
	
	@Override
	public void update(Subject s) {
		System.out.println("Android Received new CNN Notification: " + s.getState());
	}
}
