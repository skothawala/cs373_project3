package com.skkers.patterns.observer.model;

public interface Observer {
	void update(Subject s);
}
