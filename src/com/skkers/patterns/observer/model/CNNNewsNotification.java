package com.skkers.patterns.observer.model;

import java.util.ArrayList;

public class CNNNewsNotification implements Subject {

	private ArrayList<Observer> observers;
	private String notification;
	
	public CNNNewsNotification () {
		this.observers = new ArrayList<Observer>();
		this.notification = "";
	}
	
	@Override
	public void attachObserver(Observer o) {
		this.observers.add(o);
	}

	@Override
	public void detachObserver(Observer o) {
		this.observers.remove(o);
	}

	@Override
	public void notifyObservers() {
		for(Observer o: this.observers) {
			o.update(this);
		}
	}
	
	@Override
	public Object getState() {
		return (String)this.notification;
	}
	
	public void setNotification(String s) {
		this.notification = s;
	}
}
