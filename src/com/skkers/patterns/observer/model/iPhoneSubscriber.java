package com.skkers.patterns.observer.model;

public class iPhoneSubscriber implements Observer {

	@Override
	public void update(Subject s) {
		System.out.println("iPhone received new CNN Notification: " + s.getState());
	}

}
