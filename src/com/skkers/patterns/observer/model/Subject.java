package com.skkers.patterns.observer.model;

public interface Subject {
	void attachObserver(Observer o);
	void detachObserver(Observer o);
	
	void notifyObservers();
	
	Object getState();
}
