package com.skkers.patterns.observer;

import com.skkers.patterns.observer.model.AndroidSubscriber;
import com.skkers.patterns.observer.model.CNNNewsNotification;
import com.skkers.patterns.observer.model.iPhoneSubscriber;

public class ObserverExampleClient {

	public static void main(String[] args) {
		
		CNNNewsNotification cnn = new CNNNewsNotification();
		cnn.attachObserver(new iPhoneSubscriber());
		cnn.setNotification("Test 1");
		cnn.notifyObservers();
		cnn.attachObserver(new iPhoneSubscriber());
		cnn.attachObserver(new AndroidSubscriber());
		cnn.setNotification("Test 2");
		cnn.notifyObservers();
	}

}
