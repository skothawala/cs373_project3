package com.skkers.patterns.bridge;

import com.skkers.patterns.bridge.model.Color.BlueColor;
import com.skkers.patterns.bridge.model.Color.RedColor;
import com.skkers.patterns.bridge.model.Color.PurpleColor;
import com.skkers.patterns.bridge.model.Engine.BMWEngine;
import com.skkers.patterns.bridge.model.Engine.TractorEngine;
import com.skkers.patterns.bridge.model.Engine.GoKartEngine;
import com.skkers.patterns.bridge.model.ModeOfTransportation.Car;
import com.skkers.patterns.bridge.model.ModeOfTransportation.ModeOfTransportation;
import com.skkers.patterns.bridge.model.ModeOfTransportation.Tractor;
import com.skkers.patterns.bridge.model.ModeOfTransportation.GoKart;


public class BridgeExampleClient {

	public static void main(String[] args) {
		ModeOfTransportation t = new Tractor();
		t.setColor(new BlueColor());
		t.setEngine(new TractorEngine());
		
		ModeOfTransportation goKart = new Car();
		goKart.setColor(new PurpleColor());
		goKart.setEngine(new GoKartEngine());
		
		ModeOfTransportation bmw = new Car();
		bmw.setColor(new BlueColor());
		bmw.setEngine(new BMWEngine());
		
		ModeOfTransportation redBmw = new Car();
		redBmw.setColor(new RedColor());
		redBmw.setEngine(new BMWEngine());
		
		System.out.println("Tractor Color Hex (int): " + t.getColor().getHex() + "\nTractor Engine:" + t.getEngine().getHorsePower());
		System.out.println("BMW Color Hex (int): " + bmw.getColor().getHex() + "\nBMW Engine:" + bmw.getEngine().getHorsePower());
		System.out.println("redBmw ColorHex (int): " + redBmw.getColor().getHex() + "\nredBmw Engine:" + redBmw.getEngine().getHorsePower());
	}

}
