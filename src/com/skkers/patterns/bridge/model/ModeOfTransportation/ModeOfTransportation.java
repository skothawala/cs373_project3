package com.skkers.patterns.bridge.model.ModeOfTransportation;

import com.skkers.patterns.bridge.model.Color.Color;
import com.skkers.patterns.bridge.model.Engine.Engine;

public abstract class ModeOfTransportation {
	
	protected Engine engine;
	protected Color color;
	
	public abstract void move();
	
	public abstract void setEngine(Engine e);
	public abstract Engine getEngine();
	
	public abstract void setColor(Color c);
	public abstract Color getColor();
}
