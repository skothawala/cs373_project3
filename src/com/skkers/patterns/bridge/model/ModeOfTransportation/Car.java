package com.skkers.patterns.bridge.model.ModeOfTransportation;

import com.skkers.patterns.bridge.model.Color.Color;
import com.skkers.patterns.bridge.model.Engine.Engine;

public class Car extends ModeOfTransportation {
	
	public Car() {
		
	}
	
	@Override
	public void move() {
		System.out.println("Moved");		
	}

	@Override
	public void setEngine(Engine e) {
		this.engine = e;
	}

	@Override
	public Engine getEngine() {
		return this.engine;
	}

	@Override
	public void setColor(Color c) {
		this.color = c;
	}

	@Override
	public Color getColor() {
		return this.color;
	}

}
