package com.skkers.patterns.bridge.model.Engine;

public class GoKartEngine implements Engine {
	
	@Override
	public void setHorsePower(int hp) throws Exception {
		throw new Exception("This engine doesn't allow you to change HP");		
	}

	@Override
	public int getHorsePower() {
		return 18;
	}

	@Override
	public int getMaxSpeed() {
		return (int) (this.getHorsePower() * 0.8 );
	}
	
	

}
