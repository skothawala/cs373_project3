package com.skkers.patterns.bridge.model.Engine;

public interface Engine {
		
	void setHorsePower(int hp) throws Exception;
	int getHorsePower();	
	
	int getMaxSpeed();
}
