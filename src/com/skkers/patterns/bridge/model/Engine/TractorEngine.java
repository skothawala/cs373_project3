package com.skkers.patterns.bridge.model.Engine;

public class TractorEngine implements Engine{

	private int horsePower = 250;
	
	@Override
	public void setHorsePower(int hp) throws Exception {
		this.horsePower =hp;	
	}

	@Override
	public int getHorsePower() {
		return this.horsePower;
	}

	@Override
	public int getMaxSpeed() {
		return (int) (this.getHorsePower() * 0.2);
	}
	
	

}
