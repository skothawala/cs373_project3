package com.skkers.patterns.bridge.model.Color;

public class RedColor implements Color{

	@Override
	public int getHex() {
		return 16711680;
	}

}
