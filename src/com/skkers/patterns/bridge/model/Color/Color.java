package com.skkers.patterns.bridge.model.Color;

public interface Color {
	
	int getHex();
}
