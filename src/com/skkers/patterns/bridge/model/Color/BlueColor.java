package com.skkers.patterns.bridge.model.Color;

public class BlueColor implements Color {

	@Override
	public int getHex() {
		return 255;
	}

}
